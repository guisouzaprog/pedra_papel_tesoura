var jogador_escolha = 0;
var jogador_pontuacao = 0;
var computador_pontuacao = 0;
var computador_escolha = 0;
var ganhador = -1;

// 1 - pedra
// 2 - papel
// 3 - tesoura

function jogar(escolha) {
    jogador_escolha = escolha;
    computador_escolha = Math.floor((Math.random() * (3 -1 +1)) + 1);
    

    if(jogador_escolha == 1 && computador_escolha == 1) {
        ganhador = 0;
    }

    else if((jogador_escolha == 1 && computador_escolha == 2)) {
        ganhador = 1;
    }

    else if((jogador_escolha == 1 && computador_escolha == 3)) {
        ganhador = 2;
    }

    //--------
    // 1 - pedra
    // 2 - papel
    // 3 - tesoura

    else if((jogador_escolha == 2 && computador_escolha == 1)) {
        ganhador = 1;
    }

    else if((jogador_escolha == 2 && computador_escolha == 2)) {
        ganhador = 0;
    }

    else if((jogador_escolha == 2 && computador_escolha == 3)) {
        ganhador = 2;
    }

    //------ 

    else if((jogador_escolha == 3 && computador_escolha == 1)) {
        ganhador = 2;
    }

    else if((jogador_escolha == 3 && computador_escolha == 2)) {
        ganhador = 1;
    }

    else if((jogador_escolha == 3 && computador_escolha == 3)) {
        ganhador = 0;
    }

    document.getElementById("jogador_escolha-1").classList.remove("selecionado");
    document.getElementById("jogador_escolha-2").classList.remove("selecionado");
    document.getElementById("jogador_escolha-3").classList.remove("selecionado");

    document.getElementById("computador_escolha-1").classList.remove("selecionado");
    document.getElementById("computador_escolha-2").classList.remove("selecionado");
    document.getElementById("computador_escolha-3").classList.remove("selecionado");




    document.getElementById("computador_escolha-" + computador_escolha).classList.remove("selecionado");

    document.getElementById("jogador_escolha-" + jogador_escolha).classList.remove("selecionado");

    document.getElementById("jogador_escolha-" + jogador_escolha).classList.add("selecionado");

    document.getElementById("computador_escolha-" + computador_escolha).classList.add("selecionado");


if(ganhador == 0) {
    document.getElementById('mensagens').innerHTML = 'Empate';
}
else if(ganhador == 1) {
    document.getElementById('mensagens').innerHTML = 'Jogador ganhou';
    jogador_pontuacao++;
}
else if(ganhador == 2) {
    document.getElementById('mensagens').innerHTML = 'Computador ganhou';
    computador_pontuacao++;
}
document.getElementById('jogador_pontos').innerHTML = jogador_pontuacao;

document.getElementById('computador_pontos').innerHTML = computador_pontuacao;

}